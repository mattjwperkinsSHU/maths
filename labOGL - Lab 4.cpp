// Lab1.cpp 17/11/15
#include <cmath>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
using namespace std;

//#include <GL/glut.h>
// If the system can't find file glut.h, this file should be
// copied into the directory where source files are put,
// and the previous #include line replaced by
#include "glut.h"

const double PI = 4.0*atan(1.0); //3.141592654;

// Variables passed between the main program and the drawing function
// must be global, because the drawing function can't have parameters.

int noOfSides(5); // no. of sides of regular polygon to be drawn

int noOfSidesInPolygonFunction(0);

float moveit = 0;
float increment = -0.01;

float rotation = 0;

bool leftClick = false;
bool rightClick = false;

int clickedX(1);
int clickedY(1);

int innerRadius = 45;
int outerRadius = 65;

vector<int> PolygonInternalXPoints;
vector<int> PolygonInternalYPoints;

vector<int> PolygonExternalXPoints;
vector<int> PolygonExternalYPoints;

int main(int argc, char* argv[])
{
	void callbackDisplay();
	void getNoOfSides();
	void idle();

	void mouse(int button, int state, int x, int y);
	void keyboard(unsigned char key, int x, int y);

	// Initialise glut
	glutInit(&argc,argv); // initialise GLUT; the command line arguments can be ignored
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); // must be called before glutCreateWindow
	glutInitWindowSize(1000,1000); // set width and height of graphics window
	glutInitWindowPosition(500,250); // initial position of graphics window on screen
	
	// Create Window
	glutCreateWindow("A simple OpenGL program"); // create graphics window and name its heading
	glutDisplayFunc(callbackDisplay); // function to be called whenever graphics window is redrawn
	glShadeModel(GL_SMOOTH); // interpolate colours between vertices
	glClearColor(0,0,0,1); // background colour: black 
	// RGB values above.

	glutIdleFunc(idle);
	gluOrtho2D(-500,500,-500,500); 

	glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard);

	getNoOfSides(); // for regular polygon
	glutMainLoop(); // begin event-processing loop
	return 0;
}

void mouse(int button, int state, int x, int y)
{
	//myDisplay1();

	if (button == 0 && state == 00)
	{
		leftClick = !leftClick;
	}

	if (button == 2 && state == 00)
	{
		rightClick = !rightClick;
		clickedX = x;
		clickedY = y;
	}

	cout << "X:" << x << "   Y:" << y << endl;
	cout << leftClick << rightClick << endl;
	cout << button << endl;
}

void keyboard(unsigned char key, int x, int y)
{
	//myDisplay2();
}

void callbackDisplay()
{
	void example();
	void seriesOfLines();
	void polygon();
	void cube();
	
	glClear(GL_COLOR_BUFFER_BIT); // Clear graphics window (to colour set by glClearColor)
//	example();
	
	polygon();
//	seriesOfLines();	// Make a series of lines.
	//cube();
	glutSwapBuffers();
}

void idle() {
	
	moveit += increment;

	if (moveit > 15 || moveit < -20)
		increment *= -1;
	callbackDisplay();
}

void example()
{

	/*
	glColor3f(1,1,1); // set colour (white) for drawing commands (until next glColor3f call)
	glBegin(GL_POINTS); // draw a single point
		glVertex2i(300, 200);
	glEnd();
	glColor3f(0,0,1); // blue
	glBegin(GL_LINES); // draw a line
		glVertex2i(100,300);
		glVertex2i(150,100);
	glEnd();
	glLineWidth(15);
	glBegin(GL_LINES); // draw a line
	glVertex2i(600, 400);
	glVertex2i(50, 50);
	glEnd();
	glColor3f(1,1,1); // red
	glLineWidth(20);
	glBegin(GL_LINE_LOOP); // draw the edges of a polygon
		glVertex2i(500,300);

		glVertex2i(550,300);

		glVertex2i(520,250);
	glEnd();
	glColor3f(1,1,0); // yellow
	glBegin(GL_POLYGON); // draw a filled polygon
		glPointSize(10);
		glVertex2i(500,200);
		glVertex2i(550,200);
		glVertex2i(530,150);
	glEnd();
	*/
	// LAB 3.

	// YELLOW TRIANGLE.
	glPushMatrix();
	glColor3f(1, 1, 0);//yellow outline triangle
	//glTranslatef(-100, 0, 0);//translate 100 to the left

	glTranslatef(moveit, 0, 0);//translate 100 to the left
	glBegin(GL_LINE_LOOP); // draw the edges of a polygon
	glVertex2i(500, 300);
	glVertex2i(550, 300);
	glVertex2i(520, 250);
	glEnd();
	glPopMatrix();
	//glRotatef(20, 0, 0, 1);//rotate 20 degrees anticlockwise about origin



	// BLUE TRIANGLE
	glPushMatrix();
	glColor3f(0, 0, 1);//blue solid triangle
	// could push the matrix in future to isolate it.
	// MATRIX OPERATOINS (In Reverse Order Remember)
	glTranslatef(300, 200, 0);//translate back from origin
	glRotatef(45, 0, 0, 1);//rotate 45 degrees anticlockwise about origin
	glScaled(2, 2, 2);	// Scale it by 2 in X,Y and Z Axis
	glTranslatef(-300, -200, 0);//translate to origin
	// could pop the matrix, as if you don't pop it back, you're not maintaining the stack principle.
	glBegin(GL_POLYGON); // draw a filled polygon	

	glVertex2i(300, 200);
	glVertex2i(350, 200);
	glVertex2i(330, 150);
	glEnd();
	glPopMatrix();
	/*
	*/
	// Lab 4.

}

void getNoOfSides()
{
	cout << "How many sides? (Between 3 and 8, let's not get crazy...) ";
	cin >> noOfSides;
	while (noOfSides < 3 || noOfSides > 8)
	{
		cout << "Too few sides.\n";
		cout << "How many sides? ";
		cin >> noOfSides;
	}

	cout << endl << "Right click to set star position.  Left click to toggle Star or Polygon." << endl;
	cout << "Use Keys XXXXXX to rotate left and right." << endl;

	float internalAngle = 360 / noOfSides;

	for (int i(0); i < noOfSides; i++)
	{
		int xdistance = innerRadius * cos(internalAngle * (noOfSides + 1));
		int ydistance = innerRadius * sin(internalAngle * (noOfSides + 1));

		PolygonInternalXPoints.push_back(xdistance);
		PolygonInternalYPoints.push_back(ydistance);
	}

	// Half way between the X axis and the first point. We make our first outer star point.
	float startingAngle = internalAngle * 0.5;

	for (int i(0); i < noOfSides; i++)
	{
		int xoutdistance = outerRadius * cos(startingAngle * (noOfSides + 1));
		int youtdistance = outerRadius * sin(startingAngle * (noOfSides + 1));

		PolygonExternalXPoints.push_back(xoutdistance);
		PolygonExternalYPoints.push_back(youtdistance);

		startingAngle += internalAngle;
	}

}

bool written = false;
void polygon()
{

	if (!written)
		cout << "you're running the correct function at least";


	//glPushMatrix();
	//glTranslatef(clickedX, clickedY, 0);//translate somewhere
	//glBegin(GL_LINE_LOOP); // draw the edges of a polygon


	glBegin(GL_LINE_LOOP);
	glPointSize(25);
	glLineWidth(25);
	
	glColor3f(0.7, 0.7, 0.7);

	glVertex2i(-500+PolygonInternalXPoints.at(0) + clickedX, -500+PolygonInternalYPoints.at(0) + clickedY);

	for (int i(0); i < noOfSides; i++)
	{
		glVertex2i(-500+PolygonInternalXPoints.at(i) + clickedX, -500+PolygonInternalYPoints.at(i) + clickedY);


		if (leftClick)
		{
			glVertex2i(-500+PolygonExternalXPoints.at(i) + clickedX, -500+PolygonExternalYPoints.at(i) + clickedY);
		}
		if (!written)
		{
			cout << PolygonInternalXPoints.at(i) + clickedX << " " << PolygonInternalYPoints.at(i) << endl;
			cout << PolygonExternalXPoints.at(i) + clickedX << " " << PolygonExternalYPoints.at(i) << endl;
		}
	}

	glVertex2i(-500+PolygonInternalXPoints.at(0) + clickedX, -500+PolygonInternalYPoints.at(0) + clickedY);
	written = true;
	glEnd();
	//glEnd();
	//glPopMatrix();

}

void seriesOfLines()
{
	//cout << "seriesOfLines called";
	const int noOfVertices (12);
	float vertices[][noOfVertices] =
	{
		{152, 75, 74,135,152,135,321,371,247, 75,152,371}, // x coords
		{256,324,149, 29,256, 29, 80,268,323,324,256,268}  // y coords
	};
	/*
	for (int i(0); i < noOfVertices; i++)
	{
		// first float (i)
		// second float (i+1)
		// draw a line between first and second float.
		//glPushMatrix();
		glBegin(GL_LINE_LOOP); // draw the edges of a polygon
		glVertex2i(vertices[i][0], vertices[i][1]);
		glEnd();
		//glPopMatrix();
	}
	*/

}

void cube()
{
	const int noOfVertices (8);
	const float perspVertices[4][noOfVertices] =  // homogeneous coordinates of vertices
	{
	  {-144.806, -54.133,-122.762, -32.089,  32.089, 122.762,  54.133, 144.806},
	  { -58.080,-142.871, 121.829,  37.038, -37.038,-121.829, 142.871,  58.080},
	  {   0    ,   0    ,   0    ,   0    ,   0    ,   0    ,   0    ,   0    },
	  {   1.150,   0.836,   0.981,   0.667,   1.332,   1.018,   1.163,   0.849}
	};
	const int verticesPerFace (4);
	const int faces[][verticesPerFace] = // vertex indices in visible faces
	{
	  {0, 1, 3, 2},
	  {7, 3, 1, 5},
	  {7, 6, 2, 3}
	};
	const int noOfFaces (sizeof(faces)/sizeof(faces[0])); // no. of rows in the matrix
}
