// Lab1.cpp 17/11/15
#include <cmath>
#include <cstdlib>
#include <cassert>
#include <iostream>
using namespace std;

//#include <GL/glut.h>
// If the system can't find file glut.h, this file should be
// copied into the directory where source files are put,
// and the previous #include line replaced by
#include "glut.h"

const double PI = 4.0*atan(1.0); //3.141592654;

// Variables passed between the main program and the drawing function
// must be global, because the drawing function can't have parameters.
int noOfSides(5); // no. of sides of regular polygon to be drawn

float rotation = 0;

int whichFunc = 2;

void Q4D8_Verts();
void Q4D8_Illum();

int main(int argc, char* argv[])
{
	void callbackDisplay();
	void getNoOfSides();
	void idle();
	void keyboard(unsigned char key, int x, int y);

	// Initialise glut
	glutInit(&argc, argv); // initialise GLUT; the command line arguments can be ignored
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); // must be called before glutCreateWindow
	glutInitWindowSize(700, 700); // set width and height of graphics window
	glutInitWindowPosition(500, 250); // initial position of graphics window on screen


	// Create Window
	glutCreateWindow("A simple OpenGL program"); // create graphics window and name its heading
	glutDisplayFunc(callbackDisplay); // function to be called whenever graphics window is redrawn
	glShadeModel(GL_SMOOTH); // interpolate colours between vertices
	glClearColor(0, 0, 0, 1); // background colour: black 
	// RGB values above.

	// Initialising the Depth Buffer
	glEnable(GL_DEPTH_TEST);
	glutIdleFunc(idle);

	//Initialising Lighting//////////////
	/*
	*/
	
	// Light 0
	glEnable(GL_LIGHT0);

	float L0POS[] = { 2,0,0,1 };		// Light 0 Position
	float L0AMB[] = { 0.1,0.1,0.1,1 };		// Light 0 Ambient Values
	float L0DIF[] = { 0.7,0.1,0.1,1 };		// Light 0 Diffuse Values
	float L0SPEC[] = { 1,1,1,1 };			// Light 0 Specular Values

	glLightfv(GL_LIGHT0, GL_POSITION, L0POS);
	// Light 2
	glEnable(GL_LIGHT1);

	float L1POS[] = { 20,0,0,1 };		// Light 1 Position
	float L1AMB[] = { 0.0,0.0,0.0,1 };		// Light 1 Ambient Values
	float L1DIF[] = { 0.7,0.1,0.1,1 };		// Light 1 Diffuse Values
	float L1SPEC[] = { 1,1,1,1 };			// Light 1 Specular Values

	glLightfv(GL_LIGHT1, GL_POSITION, L1POS);

	// Light 3
	glEnable(GL_LIGHT2);

	float L2POS[] = { -5,0,0,1 };		// Light 2 Position
	float L2AMB[] = { 0.0,0.0,0.0,1 };		// Light 2 Ambient Values
	float L2DIF[] = { 0.1,0.7,0.1,1 };		// Light 2 Diffuse Values
	float L2SPEC[] = { 1,1,1,1 };			// Light 2 Specular Values

	glLightfv(GL_LIGHT2, GL_POSITION, L2POS);

	glEnable(GL_LIGHTING);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glOrtho(-2, 2, -2, 2, -2, 2);
	gluPerspective(45, 1, 1, 100);
	glutKeyboardFunc(keyboard);

	//	getNoOfSides(); // for regular polygon
	cout << "Press 3 to Get the Illuminated by Light option. 2 Uses a previous version.";
	glutMainLoop(); // begin event-processing loop
	return 0;
}

void keyboard(unsigned char key, int x, int y)
{
	void callbackDisplay();

	if (key == '2')
	{
		whichFunc = 2;
	}
	if (key == '3')
	{
		whichFunc = 3;
	}
		cout << whichFunc << endl;

	callbackDisplay();
}

void callbackDisplay()
{
	void example();

	//	glClear(GL_COLOR_BUFFER_BIT); // Clear graphics window (to colour set by glClearColor)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear graphics window (to colour set by glClearColor)
	example();

	glutSwapBuffers();
}

void idle() {
	callbackDisplay();
}


void example()
{
	rotation += .01;

	glPushMatrix();
	glScalef(0.5, 0.5, 0.5);
	glTranslatef(5, -5, -100);				// Move the object back 10 in the Z axis.
	glRotatef(25.264 + rotation, 1, 0, 0);
	glRotatef(40 + rotation, 0, 1, 0);
	
	glRotatef(0 + rotation, 0, 0, 1);
	
	//glShadeModel(GL_FLAT);
	glShadeModel(GL_SMOOTH);

	switch (whichFunc)
	{
		//case 1:	Q4D8_Faces(); break;
		case 2:	Q4D8_Verts(); break;
		case 3:	Q4D8_Illum(); break;
	}
	
	//new3DCube();
	glPopMatrix();

}


void Q4D8_Verts()
{

	// 8 Verts in the Cube.
	const int howManyVerts = 6;
	const int D8Verts[][howManyVerts] = // Because we don't say 3 in the first box, we can later use this for homogenous coords as well?
	{
		{	35.95176185, -17.97588093, 7.001975288, -10.50296293, 5.276366837, -5.276366837},
		{ 0,	0,	12.03630046, -18.05445069, -15.9727102,	15.9727102 },
		{ -17.53484587,	8.767422936,	14.35617683, -21.53426525,	10.8181552, -10.8181552 }
		/*
		{ 40, -20,	0,	0,	0,	0 },
		{ 0,	0,	20, -30,	0,	0 },
		{ 0,	0,	0,	0,	20, -20 }
		*/

	};


	// 6 Faces
	const int howManyFaces = 8;
	const int facesOrder[][howManyFaces] =
	{
		{ 2,4,0 },	// Face 1
		{ 3,0,4 },	// Face 2
		{ 3,4,1 },	// Face 3
		{ 2,1,4 },	// Face 4
		{ 2,0,5 },	// Face 5
		{ 3,5,0 },	// Face 6
		{ 3,1,5 },	// Face 7
		{ 2,5,1 }	// Face 8
					//...
	};

	const float colours[3][8] =
	{
		{ 1	,	0,	0.4,	0.5,	3,	1,	1,	1 },
		{ 1	,	0,	1,		0.5,	1,	0,	1,	0 },
		{ 1	,	1,	0.1,	0.7,	7,	1,	0,	0 }
	};



	// Change the format over
	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < howManyFaces; facenumber++)	// Number of Faces.
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = facesOrder[facenumber][0];
		int vert2 = facesOrder[facenumber][1];
		int vert3 = facesOrder[facenumber][2];
		//int vert4 = facesOrder[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = D8Verts[0][vert1];
		float point1y = D8Verts[1][vert1];
		float point1z = D8Verts[2][vert1];

		float point2x = D8Verts[0][vert2];
		float point2y = D8Verts[1][vert2];
		float point2z = D8Verts[2][vert2];

		float point3x = D8Verts[0][vert3];
		float point3y = D8Verts[1][vert3];
		float point3z = D8Verts[2][vert3];

		//float point4x = cubeVerts[0][vert4];
		//float point4y = cubeVerts[1][vert4];
		//float point4z = cubeVerts[2][vert4];

		// This needs updating

		//glColor3f(1, 1, 1);				// White
		//glColor3f(colours[0][facenumber], colours[1][facenumber], colours[2][facenumber]);


		//glBegin(GL_LINE_LOOP);
		glBegin(GL_TRIANGLES);
		glColor3f(colours[0][vert1], colours[1][vert1], colours[2][vert1]);
		glVertex3f(point1x, point1y, point1z);
		glColor3f(colours[0][vert2], colours[1][vert2], colours[2][vert2]);
		glVertex3f(point2x, point2y, point2z);
		glColor3f(colours[0][vert3], colours[1][vert3], colours[2][vert3]);
		glVertex3f(point3x, point3y, point3z);
		glEnd();

	}
	//		glPopMatrix();
}


void Q4D8_Illum()
{
	const int howManyVerts = 6;
	const int D8Verts[][howManyVerts] = // Because we don't say 3 in the first box, we can later use this for homogenous coords as well?
	{
		{	35.95176185, -17.97588093, 7.001975288, -10.50296293, 5.276366837, -5.276366837},
		{ 0,			0,				12.03630046, -18.05445069, -15.9727102,	15.9727102 },
		{ -17.53484587,	8.767422936,	14.35617683, -21.53426525,	10.8181552, -10.8181552 }
	};


	
	const int howManyFaces = 8;
	
	// This lists the vertices of each face in anti-clockwise order.
	const int facesOrder[][howManyFaces] =
	{
		{ 2,4,0 },	// Face 1
		{ 3,0,4 },	// Face 2
		{ 3,4,1 },	// Face 3
		{ 2,1,4 },	// Face 4
		{ 2,0,5 },	// Face 5
		{ 3,5,0 },	// Face 6
		{ 3,1,5 },	// Face 7
		{ 2,5,1 }	// Face 8
						};

	const float colours[3][8] =
	{
		{ 1	,	0,	0.4,	0.5,	3,	1,	1,	1 },
		{ 1	,	0,	1,		0.5,	1,	0,	1,	0 },
		{ 1	,	1,	0.1,	0.7,	7,	1,	0,	0 }
	};

/*
const float vertexNormals[3][howManyVerts] = 
	{
		{ 1,2,3,4,5,6 },
		{ 1,2,3,4,5,6 },
		{ 1,2,3,4,5,6 }
	};
*/
	float faceNormals[3][8] =
	{
		{ 850.6513035, 575.7794265,		-521.0249284,	-113.950776,	428.5419566,	-57.38459397, -837.6069387, -325.0054495 },
		{ -157.4563895, -1439.814631, -719.9073153, -78.72819476,	1120.360427,	476.9105935,	238.4552968,	560.1802133 },
		{ 831.6248224, -188.1804496,	300.4438073,	678.8350993, -33.82759327, -1486.359073,	-348.6455044,	246.1088914 }
	};

	 //Change the format over
	/////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////


	 //For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < howManyFaces; facenumber++)	// Number of Faces.
	{
		//Which vertices are we dealing with for this face? facesOrder tells us.
		int vert1 = facesOrder[facenumber][0];
		int vert2 = facesOrder[facenumber][1];
		int vert3 = facesOrder[facenumber][2];

		//For each vertices, we need an X, Y and Z.
		float point1x = D8Verts[0][vert1];
		float point1y = D8Verts[1][vert1];
		float point1z = D8Verts[2][vert1];

		float point2x = D8Verts[0][vert2];
		float point2y = D8Verts[1][vert2];
		float point2z = D8Verts[2][vert2];

		float point3x = D8Verts[0][vert3];
		float point3y = D8Verts[1][vert3];
		float point3z = D8Verts[2][vert3];

		float faceAmbient[] = { colours[0][vert1],colours[1][vert1],colours[2][vert1],1 };	
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, faceAmbient);
		float faceDiffuse[] = { colours[0][vert1],colours[1][vert1],colours[2][vert1],1 };		
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, faceDiffuse);
		float faceSpecular[] = { 0.2,0.2,0.2,1 };		
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, faceSpecular);

		float specularExponent(75);


		glBegin(GL_TRIANGLES);
		//glNormal3f(vertexNormals[0][vert1],);
		glNormal3f(faceNormals[0][facenumber], faceNormals[1][facenumber], faceNormals[2][facenumber]);
		glColor3f(colours[0][vert1], colours[1][vert1], colours[2][vert1]);
		glVertex3f(point1x, point1y, point1z);
		
		glNormal3f(faceNormals[0][facenumber], faceNormals[1][facenumber], faceNormals[2][facenumber]);
		glColor3f(colours[0][vert2], colours[1][vert2], colours[2][vert2]);
		glVertex3f(point2x, point2y, point2z);
		
		glNormal3f(faceNormals[0][facenumber], faceNormals[1][facenumber], faceNormals[2][facenumber]);
		glColor3f(colours[0][vert3], colours[1][vert3], colours[2][vert3]);
		glVertex3f(point3x, point3y, point3z);
		glEnd();

	}
			glPopMatrix();
}

