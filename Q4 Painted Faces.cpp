// Lab1.cpp 17/11/15
#include <cmath>
#include <cstdlib>
#include <cassert>
#include <iostream>
using namespace std;

//#include <GL/glut.h>
// If the system can't find file glut.h, this file should be
// copied into the directory where source files are put,
// and the previous #include line replaced by
#include "glut.h"

const double PI = 4.0*atan(1.0); //3.141592654;

// Variables passed between the main program and the drawing function
// must be global, because the drawing function can't have parameters.
int noOfSides(5); // no. of sides of regular polygon to be drawn

float moveit = 0;
float increment = -0.4;

float rotation = 0;

int whichFunc = 1;

void Q4D8_Faces();
void Q4D8_Verts();
void Q4D8_Illum();

int main(int argc, char* argv[])
{
	void callbackDisplay();
	void getNoOfSides();
	void idle();
	void keyboard(unsigned char key, int x, int y);

	// Initialise glut
	glutInit(&argc, argv); // initialise GLUT; the command line arguments can be ignored
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); // must be called before glutCreateWindow
	glutInitWindowSize(700, 700); // set width and height of graphics window
	glutInitWindowPosition(500, 250); // initial position of graphics window on screen


	// Create Window
	glutCreateWindow("A simple OpenGL program"); // create graphics window and name its heading
	glutDisplayFunc(callbackDisplay); // function to be called whenever graphics window is redrawn
	glShadeModel(GL_SMOOTH); // interpolate colours between vertices
	glClearColor(0, 0, 0, 1); // background colour: black 
	// RGB values above.

	// Initialising the Depth Buffer
	glEnable(GL_DEPTH_TEST);
	glutIdleFunc(idle);

	// Orthographic
	//gluOrtho2D(0,699,0,699); 

	// Lighting Initialisation.
	//glEnable(GL_LIGHTING);


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glOrtho(-2, 2, -2, 2, -2, 2);
	gluPerspective(45, 1, 1, 100);
	glutKeyboardFunc(keyboard);

	//	getNoOfSides(); // for regular polygon
	glutMainLoop(); // begin event-processing loop
	return 0;
}

void keyboard(unsigned char key, int x, int y)
{
	void callbackDisplay();

	if (key == '1')
	{
		whichFunc = 1;
	}
	if (key == '2')
	{
		whichFunc = 2;
	}
	if (key == '3')
	{
		whichFunc = 3;
	}
		cout << whichFunc << endl;

	callbackDisplay();
}

void callbackDisplay()
{
	void example();
	void seriesOfLines();
	void polygon();
	void cube();


	//	glClear(GL_COLOR_BUFFER_BIT); // Clear graphics window (to colour set by glClearColor)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear graphics window (to colour set by glClearColor)
	example();

	//polygon();
	//seriesOfLines();	// Make a series of lines.
	//cube();
	glutSwapBuffers();
}

void idle() {

	moveit += increment;

	if (moveit > 15 || moveit < -20)
		increment *= -1;
	callbackDisplay();
}


void example()
{
	void new3DCube();

	rotation += 0.1;

	glPushMatrix();
	glScalef(0.5, 0.5, 0.5);
	glTranslatef(5, -5, -100);				// Move the object back 10 in the Z axis.
//	glRotatef(25.264 + rotation, 1, 0, 0);
//	glRotatef(40 + rotation, 0, 1, 0);
	
	glRotatef(rotation, 0.5, 0.5, 0);
	//glRotatef(0 + rotation, 0, 0, 1);
	
	switch (whichFunc)
	{
		case 1:	Q4D8_Faces(); break;
		case 2:	Q4D8_Verts(); break;
		case 3:	Q4D8_Illum(); break;
	}
	
	//new3DCube();
	glPopMatrix();

}



void getNoOfSides()
{
	cout << "How many sides? ";
	cin >> noOfSides;
	while (noOfSides < 3)
	{
		cout << "Too few sides.\n";
		cout << "How many sides? ";
		cin >> noOfSides;
	}
}

void polygon()
{
}

void seriesOfLines()
{
	//cout << "seriesOfLines called";
	const int noOfVertices(12);
	float vertices[][noOfVertices] =
	{
		{152, 75, 74,135,152,135,321,371,247, 75,152,371}, // x coords
		{256,324,149, 29,256, 29, 80,268,323,324,256,268}  // y coords
	};

	glPushMatrix();
	glTranslatef(300, 200 + moveit, 0);//translate somewhere
	glBegin(GL_LINE_LOOP); // draw the edges of a polygon
	//glPointSize(25);
	//glLineWidth(25);
	for (int i(0); i < noOfVertices; i++)
	{
		// first float (i)
		// second float (i+1)
		// draw a line between first and second float.
		glVertex2i(vertices[0][i], vertices[1][i]);
	}
	glEnd();
	glPopMatrix();
	/*
	*/

}

void cube()
{
	const int noOfVertices(8);
	const float perspVertices[4][noOfVertices] =  // homogeneous coordinates of vertices
	{
	  {-144.806, -54.133,-122.762, -32.089,  32.089, 122.762,  54.133, 144.806},
	  { -58.080,-142.871, 121.829,  37.038, -37.038,-121.829, 142.871,  58.080},
	  {   0    ,   0    ,   0    ,   0    ,   0    ,   0    ,   0    ,   0    },
	  {   1.150,   0.836,   0.981,   0.667,   1.332,   1.018,   1.163,   0.849}
	};
	const int verticesPerFace(4);
	const int faces[][verticesPerFace] = // vertex indices in visible faces
	{
	  {0, 1, 3, 2},
	  {7, 3, 1, 5},
	  {7, 6, 2, 3}
	};
	const int noOfFaces(sizeof(faces) / sizeof(faces[0])); // no. of rows in the matrix

	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////

	glPushMatrix();
	glTranslatef(300 + moveit, 400, 0);//translate somewhere

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < noOfFaces; facenumber++)
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = faces[facenumber][0];
		int vert2 = faces[facenumber][1];
		int vert3 = faces[facenumber][2];
		int vert4 = faces[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = perspVertices[0][vert1] / perspVertices[3][vert1];
		float point1y = perspVertices[1][vert1] / perspVertices[3][vert1];

		float point2x = perspVertices[0][vert2] / perspVertices[3][vert2];
		float point2y = perspVertices[1][vert2] / perspVertices[3][vert2];

		float point3x = perspVertices[0][vert3] / perspVertices[3][vert3];
		float point3y = perspVertices[1][vert3] / perspVertices[3][vert3];

		float point4x = perspVertices[0][vert4] / perspVertices[3][vert4];
		float point4y = perspVertices[1][vert4] / perspVertices[3][vert4];

		glColor3f(1, 1, 1);				// White
		glBegin(GL_LINE_LOOP);
		glVertex2i(point1x, point1y);
		glVertex2i(point2x, point2y);
		glVertex2i(point3x, point3y);
		glVertex2i(point4x, point4y);
		glEnd();
	}
	glPopMatrix();
}




void new3DCube()
{

	// 8 Verts in the Cube.
	const int howWide = 8;
	const int cubeVerts[][howWide] = // Because we don't say 3 in the first box, we can later use this for homogenous coords as well?
	{
	{-1, -1, 1, 1, -1, -1, 1, 1},
	{-1, 1, 1, -1, -1, 1, 1, -1},
	{1, 1, 1, 1, -1, -1, -1, -1}
	};

	// 6 Faces
	const int howWideNow = 6;
	const int facesOrder[][howWideNow] =
	{
	{0,3,2,1},	// Face 0
	{2,3,7,6},	// Face 1
	{3,0,4,7},	// Face 2
	{1,2,6,5},	// Face 3
	{4,5,6,7},	// Face 4
	{5,4,0,1}	// Face 5
	//...
	};

	const float colours[3][8] =
	{
		{1,0,0,0,1,1,1,1},
		{1,0,1,1,0,0,1,0},
		{1,1,0,1,0,1,0,0}
	};



	// Change the format over
	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < howWideNow; facenumber++)	// Number of Faces.
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = facesOrder[facenumber][0];
		int vert2 = facesOrder[facenumber][1];
		int vert3 = facesOrder[facenumber][2];
		int vert4 = facesOrder[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = cubeVerts[0][vert1];
		float point1y = cubeVerts[1][vert1];
		float point1z = cubeVerts[2][vert1];

		float point2x = cubeVerts[0][vert2];
		float point2y = cubeVerts[1][vert2];
		float point2z = cubeVerts[2][vert2];

		float point3x = cubeVerts[0][vert3];
		float point3y = cubeVerts[1][vert3];
		float point3z = cubeVerts[2][vert3];

		float point4x = cubeVerts[0][vert4];
		float point4y = cubeVerts[1][vert4];
		float point4z = cubeVerts[2][vert4];

		// This needs updating

		//glColor3f(1, 1, 1);				// White
		//glColor3f(colours[0][facenumber], colours[1][facenumber], colours[2][facenumber]);


		//glBegin(GL_LINE_LOOP);
		glBegin(GL_TRIANGLES);
		glColor3f(colours[0][vert1], colours[1][vert1], colours[2][vert1]);
		glVertex3f(point1x, point1y, point1z);
		glColor3f(colours[0][vert2], colours[1][vert2], colours[2][vert2]);
		glVertex3f(point2x, point2y, point2z);
		glColor3f(colours[0][vert3], colours[1][vert3], colours[2][vert3]);
		glVertex3f(point3x, point3y, point3z);
		glVertex3f(point3x, point3y, point3z);
		glColor3f(colours[0][vert4], colours[1][vert4], colours[2][vert4]);
		glVertex3f(point4x, point4y, point4z);
		glColor3f(colours[0][vert1], colours[1][vert1], colours[2][vert1]);
		glVertex3f(point1x, point1y, point1z);
		glEnd();

	}
	//		glPopMatrix();
}


void Q4D8_Faces()
{

	// 8 Verts in the Cube.
	const int howManyVerts = 6;
	const int D8Verts[][howManyVerts] = // Because we don't say 3 in the first box, we can later use this for homogenous coords as well?
	{
		{	35.95176185, -17.97588093, 7.001975288, -10.50296293, 5.276366837, -5.276366837},
		{ 0,	0,	12.03630046, -18.05445069, -15.9727102,	15.9727102 },
		{ -17.53484587,	8.767422936,	14.35617683, -21.53426525,	10.8181552, -10.8181552 }
		/*
		{40, - 20,	0,	0,	0,	0},
		{0,	0,	20, - 30,	0,	0},
		{0,	0,	0,	0,	20, - 20}
		*/

	};


	// 6 Faces
	const int howManyFaces = 8;
	const int facesOrder[][howManyFaces] =
	{
		{ 2,4,0 },	// Face 1
		{ 3,0,4 },	// Face 2
		{ 3,4,1 },	// Face 3
		{ 2,1,4 },	// Face 4
		{ 2,0,5 },	// Face 5
		{ 3,5,0 },	// Face 6
		{ 3,1,5 },	// Face 7
		{ 2,5,1 }	// Face 8
					//...
	};

	const float colours[3][8] =
	{
		{ 1	,	0,	0.4,	0.5,	3,	1,	1,	1 },
		{ 1	,	0,	1,		0.5,	1,	0,	1,	0 },
		{ 1	,	1,	0.1,	0.7,	7,	1,	0,	0 }
	};



	// Change the format over
	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < howManyFaces; facenumber++)	// Number of Faces.
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = facesOrder[facenumber][0];
		int vert2 = facesOrder[facenumber][1];
		int vert3 = facesOrder[facenumber][2];
		//int vert4 = facesOrder[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = D8Verts[0][vert1];
		float point1y = D8Verts[1][vert1];
		float point1z = D8Verts[2][vert1];

		float point2x = D8Verts[0][vert2];
		float point2y = D8Verts[1][vert2];
		float point2z = D8Verts[2][vert2];

		float point3x = D8Verts[0][vert3];
		float point3y = D8Verts[1][vert3];
		float point3z = D8Verts[2][vert3];

		//glBegin(GL_LINE_LOOP);
		glBegin(GL_TRIANGLES);
		glColor3f(colours[0][facenumber], colours[1][facenumber], colours[2][facenumber]);
		glVertex3f(point1x, point1y, point1z);
		//glColor3f(colours[0][vert2], colours[1][vert2], colours[2][vert2]);
		glVertex3f(point2x, point2y, point2z);
		//glColor3f(colours[0][vert3], colours[1][vert3], colours[2][vert3]);
		glVertex3f(point3x, point3y, point3z);
		glEnd();

	}
	//		glPopMatrix();
}

void Q4D8_Verts()
{

	// 8 Verts in the Cube.
	const int howManyVerts = 6;
	const int D8Verts[][howManyVerts] = // Because we don't say 3 in the first box, we can later use this for homogenous coords as well?
	{
		{	35.95176185, -17.97588093, 7.001975288, -10.50296293, 5.276366837, -5.276366837},
		{ 0,	0,	12.03630046, -18.05445069, -15.9727102,	15.9727102 },
		{ -17.53484587,	8.767422936,	14.35617683, -21.53426525,	10.8181552, -10.8181552 }
		/*
		{ 40, -20,	0,	0,	0,	0 },
		{ 0,	0,	20, -30,	0,	0 },
		{ 0,	0,	0,	0,	20, -20 }
		*/

	};


	// 6 Faces
	const int howManyFaces = 8;
	const int facesOrder[][howManyFaces] =
	{
		{ 2,4,0 },	// Face 1
		{ 3,0,4 },	// Face 2
		{ 3,4,1 },	// Face 3
		{ 2,1,4 },	// Face 4
		{ 2,0,5 },	// Face 5
		{ 3,5,0 },	// Face 6
		{ 3,1,5 },	// Face 7
		{ 2,5,1 }	// Face 8
					//...
	};

	const float colours[3][8] =
	{
		{ 1	,	0,	0.4,	0.5,	3,	1,	1,	1 },
		{ 1	,	0,	1,		0.5,	1,	0,	1,	0 },
		{ 1	,	1,	0.1,	0.7,	7,	1,	0,	0 }
	};



	// Change the format over
	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < howManyFaces; facenumber++)	// Number of Faces.
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = facesOrder[facenumber][0];
		int vert2 = facesOrder[facenumber][1];
		int vert3 = facesOrder[facenumber][2];
		//int vert4 = facesOrder[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = D8Verts[0][vert1];
		float point1y = D8Verts[1][vert1];
		float point1z = D8Verts[2][vert1];

		float point2x = D8Verts[0][vert2];
		float point2y = D8Verts[1][vert2];
		float point2z = D8Verts[2][vert2];

		float point3x = D8Verts[0][vert3];
		float point3y = D8Verts[1][vert3];
		float point3z = D8Verts[2][vert3];

		//float point4x = cubeVerts[0][vert4];
		//float point4y = cubeVerts[1][vert4];
		//float point4z = cubeVerts[2][vert4];

		// This needs updating

		//glColor3f(1, 1, 1);				// White
		//glColor3f(colours[0][facenumber], colours[1][facenumber], colours[2][facenumber]);


		//glBegin(GL_LINE_LOOP);
		glBegin(GL_TRIANGLES);
		glColor3f(colours[0][vert1], colours[1][vert1], colours[2][vert1]);
		glVertex3f(point1x, point1y, point1z);
		glColor3f(colours[0][vert2], colours[1][vert2], colours[2][vert2]);
		glVertex3f(point2x, point2y, point2z);
		glColor3f(colours[0][vert3], colours[1][vert3], colours[2][vert3]);
		glVertex3f(point3x, point3y, point3z);
		glEnd();

	}
	//		glPopMatrix();
}

void Q4D8_Illum()
{

	// 8 Verts in the Cube.
	const int howManyVerts = 6;
	const int D8Verts[][howManyVerts] = // Because we don't say 3 in the first box, we can later use this for homogenous coords as well?
	{
		{	35.95176185, -17.97588093, 7.001975288, -10.50296293, 5.276366837, -5.276366837},
		{ 0,	0,	12.03630046, -18.05445069, -15.9727102,	15.9727102 },
		{ -17.53484587,	8.767422936,	14.35617683, -21.53426525,	10.8181552, -10.8181552 }
		/*
		{ 40, -20,	0,	0,	0,	0 },
		{ 0,	0,	20, -30,	0,	0 },
		{ 0,	0,	0,	0,	20, -20 }
		*/

	};


	// 6 Faces
	const int howManyFaces = 8;
	const int facesOrder[][howManyFaces] =
	{
		{ 2,4,0 },	// Face 1
		{ 3,0,4 },	// Face 2
		{ 3,4,1 },	// Face 3
		{ 2,1,4 },	// Face 4
		{ 2,0,5 },	// Face 5
		{ 3,5,0 },	// Face 6
		{ 3,1,5 },	// Face 7
		{ 2,5,1 }	// Face 8
					//...
	};

	const float colours[3][8] =
	{
		{ 1	,	0,	0.4,	0.5,	3,	1,	1,	1 },
		{ 1	,	0,	1,		0.5,	1,	0,	1,	0 },
		{ 1	,	1,	0.1,	0.7,	7,	1,	0,	0 }
	};



	// Change the format over
	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < howManyFaces; facenumber++)	// Number of Faces.
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = facesOrder[facenumber][0];
		int vert2 = facesOrder[facenumber][1];
		int vert3 = facesOrder[facenumber][2];
		//int vert4 = facesOrder[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = D8Verts[0][vert1];
		float point1y = D8Verts[1][vert1];
		float point1z = D8Verts[2][vert1];

		float point2x = D8Verts[0][vert2];
		float point2y = D8Verts[1][vert2];
		float point2z = D8Verts[2][vert2];

		float point3x = D8Verts[0][vert3];
		float point3y = D8Verts[1][vert3];
		float point3z = D8Verts[2][vert3];

		//float point4x = cubeVerts[0][vert4];
		//float point4y = cubeVerts[1][vert4];
		//float point4z = cubeVerts[2][vert4];

		// This needs updating

		//glColor3f(1, 1, 1);				// White
		//glColor3f(colours[0][facenumber], colours[1][facenumber], colours[2][facenumber]);


		//glBegin(GL_LINE_LOOP);
		glBegin(GL_TRIANGLES);
		glColor3f(colours[0][vert1], colours[1][vert1], colours[2][vert1]);
		glVertex3f(point1x, point1y, point1z);
		glColor3f(colours[0][vert2], colours[1][vert2], colours[2][vert2]);
		glVertex3f(point2x, point2y, point2z);
		glColor3f(colours[0][vert3], colours[1][vert3], colours[2][vert3]);
		glVertex3f(point3x, point3y, point3z);
		glEnd();

	}
	//		glPopMatrix();
}
