// Lab1.cpp 17/11/15
#include <cmath>
#include <cstdlib>
#include <cassert>
#include <iostream>
using namespace std;

//#include <GL/glut.h>
// If the system can't find file glut.h, this file should be
// copied into the directory where source files are put,
// and the previous #include line replaced by
#include "glut.h"

const double PI = 4.0*atan(1.0); //3.141592654;

// Variables passed between the main program and the drawing function
// must be global, because the drawing function can't have parameters.
int noOfSides (5); // no. of sides of regular polygon to be drawn

float moveit = 0;
float increment = -0.01;

int main(int argc, char* argv[])
{
	void callbackDisplay();
	void getNoOfSides();
	void idle();

	// Initialise glut
	glutInit(&argc,argv); // initialise GLUT; the command line arguments can be ignored
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); // must be called before glutCreateWindow
	glutInitWindowSize(700,700); // set width and height of graphics window
	glutInitWindowPosition(500,250); // initial position of graphics window on screen
	
	// Create Window
	glutCreateWindow("A simple OpenGL program"); // create graphics window and name its heading
	glutDisplayFunc(callbackDisplay); // function to be called whenever graphics window is redrawn
	glShadeModel(GL_SMOOTH); // interpolate colours between vertices
	glClearColor(0,0,0,1); // background colour: black 
	// RGB values above.

	glutIdleFunc(idle);
	gluOrtho2D(0,699,0,699); 

//	getNoOfSides(); // for regular polygon
	glutMainLoop(); // begin event-processing loop
	return 0;
}

void callbackDisplay()
{
	void example();
	void seriesOfLines();
	void polygon();
	void cube();
	
	glClear(GL_COLOR_BUFFER_BIT); // Clear graphics window (to colour set by glClearColor)
	example();
	polygon();
	seriesOfLines();	// Make a series of lines.
	cube();
	glutSwapBuffers();
}

void idle() {
	
	moveit += increment;

	if (moveit > 15 || moveit < -20)
		increment *= -1;
	callbackDisplay();
}

void example()
{
	
	/*
	glColor3f(1,1,1); // set colour (white) for drawing commands (until next glColor3f call)
	glBegin(GL_POINTS); // draw a single point
		glVertex2i(300, 200);
	glEnd();
	glColor3f(0,0,1); // blue
	glBegin(GL_LINES); // draw a line
		glVertex2i(100,300);
		glVertex2i(150,100);
	glEnd();
	glLineWidth(15);
	glBegin(GL_LINES); // draw a line
	glVertex2i(600, 400);
	glVertex2i(50, 50);
	glEnd();
	glColor3f(1,1,1); // red
	glLineWidth(20);
	glBegin(GL_LINE_LOOP); // draw the edges of a polygon
		glVertex2i(500,300);
		
		glVertex2i(550,300);
		
		glVertex2i(520,250);
	glEnd();
	glColor3f(1,1,0); // yellow
	glBegin(GL_POLYGON); // draw a filled polygon
		glPointSize(10);
		glVertex2i(500,200);
		glVertex2i(550,200);
		glVertex2i(530,150);
	glEnd();
	*/
	// LAB 3.

	// YELLOW TRIANGLE.
	glPushMatrix();
	glColor3f(1, 1, 0);//yellow outline triangle
	//glTranslatef(-100, 0, 0);//translate 100 to the left

	glTranslatef(moveit, 0, 0);//translate 100 to the left
	glBegin(GL_LINE_LOOP); // draw the edges of a polygon
	glVertex2i(500, 300);
	glVertex2i(550, 300);
	glVertex2i(520, 250);
	glEnd();
	glPopMatrix();
	//glRotatef(20, 0, 0, 1);//rotate 20 degrees anticlockwise about origin
	
	
	
	// BLUE TRIANGLE
	glPushMatrix();
	glColor3f(0, 0, 1);//blue solid triangle
	// could push the matrix in future to isolate it.
	// MATRIX OPERATOINS (In Reverse Order Remember)
	glTranslatef(300, 200, 0);//translate back from origin
	glRotatef(45, 0, 0, 1);//rotate 45 degrees anticlockwise about origin
	glScaled(2, 2, 2);	// Scale it by 2 in X,Y and Z Axis
	glTranslatef(-300, -200, 0);//translate to origin
	// could pop the matrix, as if you don't pop it back, you're not maintaining the stack principle.
	glBegin(GL_POLYGON); // draw a filled polygon	

	glVertex2i(300, 200);
	glVertex2i(350, 200);
	glVertex2i(330, 150);
	glEnd();
	glPopMatrix();
	/*
	*/
	// Lab 4.

}



void getNoOfSides()
{
	cout << "How many sides? ";
	cin >> noOfSides;
	while (noOfSides < 3)
	{
		cout << "Too few sides.\n";
		cout << "How many sides? ";
		cin >> noOfSides;
	}
}

void polygon()
{
}

void seriesOfLines()
{
	//cout << "seriesOfLines called";
	const int noOfVertices (12);
	float vertices[][noOfVertices] =
	{
		{152, 75, 74,135,152,135,321,371,247, 75,152,371}, // x coords
		{256,324,149, 29,256, 29, 80,268,323,324,256,268}  // y coords
	};
	
		glPushMatrix();
		glTranslatef(300, 200 + moveit, 0);//translate somewhere
		glBegin(GL_LINE_LOOP); // draw the edges of a polygon
		//glPointSize(25);
		//glLineWidth(25);
	for (int i(0); i < noOfVertices; i++)
	{
		// first float (i)
		// second float (i+1)
		// draw a line between first and second float.
		glVertex2i(vertices[0][i], vertices[1][i]);
	}
		glEnd();
		glPopMatrix();
	/*
	*/

}

void cube()
{
	const int noOfVertices (8);
	const float perspVertices[4][noOfVertices] =  // homogeneous coordinates of vertices
	{
	  {-144.806, -54.133,-122.762, -32.089,  32.089, 122.762,  54.133, 144.806},
	  { -58.080,-142.871, 121.829,  37.038, -37.038,-121.829, 142.871,  58.080},
	  {   0    ,   0    ,   0    ,   0    ,   0    ,   0    ,   0    ,   0    },
	  {   1.150,   0.836,   0.981,   0.667,   1.332,   1.018,   1.163,   0.849}
	};
	const int verticesPerFace (4);
	const int faces[][verticesPerFace] = // vertex indices in visible faces
	{
	  {0, 1, 3, 2},
	  {7, 3, 1, 5},
	  {7, 6, 2, 3}
	};
	const int noOfFaces (sizeof(faces)/sizeof(faces[0])); // no. of rows in the matrix

	// Change the format over
	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////
	
	glPushMatrix();
	glTranslatef(300 + moveit, 400, 0);//translate somewhere

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < noOfFaces; facenumber++)
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = faces[facenumber][0];
		int vert2 = faces[facenumber][1];
		int vert3 = faces[facenumber][2];
		int vert4 = faces[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = perspVertices[0][vert1] / perspVertices[3][vert1];
		float point1y = perspVertices[1][vert1] / perspVertices[3][vert1];

		float point2x = perspVertices[0][vert2] / perspVertices[3][vert2];
		float point2y = perspVertices[1][vert2] / perspVertices[3][vert2];

		float point3x = perspVertices[0][vert3] / perspVertices[3][vert3];
		float point3y = perspVertices[1][vert3] / perspVertices[3][vert3];

		float point4x = perspVertices[0][vert4] / perspVertices[3][vert4];
		float point4y = perspVertices[1][vert4] / perspVertices[3][vert4];
		
		glColor3f(1, 1, 1);				// White
		glBegin(GL_LINE_LOOP);
		glVertex2i(point1x, point1y);
		glVertex2i(point2x, point2y);
		glVertex2i(point3x, point3y);
		glVertex2i(point4x, point4y);
		glEnd();
	}
		glPopMatrix();
}


/*

void new3DCube()
{

A new function. so it needs a prototype.

8 Vertices of a cube in 3D, not 2.

const int faces[][verticesPerFace] = // vertex indices in visible faces
int howWide = numberOfVerts;
const int cubeVerts[][howWide] = // Because we don't say 3 in the first box, we can later use this for homogenous coords as well.
{
{0, 1, 3, 2},
{7, 3, 1, 5},
{7, 6, 2, 3}
};


vertsPerFace?
howWideNow = vertsPerFace;
const int facesOrder[][howWideNow] = 
{
{0,3,2,1}	// Face 0
{2,3,7,6}	// Face 1
{3,0,4,7}	// Face 2
{1,2,6,5}	// Face 3
{4,5,6,7}	// Face 4
{5,4,0,1}	// Face 5

//...
}


}
*/
