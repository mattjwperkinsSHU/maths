// Lab1.cpp 17/11/15
#include <cmath>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

using namespace std;

//#include <GL/glut.h>
// If the system can't find file glut.h, this file should be
// copied into the directory where source files are put,
// and the previous #include line replaced by
#include "glut.h"

const double PI = 4.0*atan(1.0); //3.141592654;

// Variables passed between the main program and the drawing function
// must be global, because the drawing function can't have parameters.

int noOfSides(5); // no. of sides of regular polygon to be drawn

int noOfSidesInPolygonFunction(0);

float moveit = 0;
float increment = -0.01;

float rotation = 0;

bool leftClick = false;
bool rightClick = false;

int clickedX(1);
int clickedY(1);

int innerRadius = 45;
int outerRadius = 65;

vector<int> PolygonInternalXPoints;
vector<int> PolygonInternalYPoints;

vector<int> PolygonExternalXPoints;
vector<int> PolygonExternalYPoints;

void callbackDisplay();

int main(int argc, char* argv[])
{
	void getNoOfSides();
	void idle();
	void mouse(int button, int state, int x, int y);
	void keyboard(unsigned char key, int x, int y);


	// Initialise glut
	glutInit(&argc, argv); // initialise GLUT; the command line arguments can be ignored
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); // must be called before glutCreateWindow
	glutInitWindowSize(700, 700); // set width and height of graphics window
	glutInitWindowPosition(500, 250); // initial position of graphics window on screen


	// Create Window
	glutCreateWindow("A simple OpenGL program"); // create graphics window and name its heading
	glutDisplayFunc(callbackDisplay); // function to be called whenever graphics window is redrawn
	glShadeModel(GL_SMOOTH); // interpolate colours between vertices
	glClearColor(0, 0, 0, 1); // background colour: black 
	// RGB values above.

	// Initialising the Depth Buffer
	glEnable(GL_DEPTH_TEST);

	//Initialising Lighting//////////////
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	float L0POS[] = { 2,0,0,1 };		// Light 0 Position
	float L0AMB[] = { 0.1,0.1,0.1,1 };		// Light 0 Ambient Values
	float L0DIF[] = { 0.7,0.1,0.1,1 };		// Light 0 Diffuse Values
	float L0SPEC[] = { 1,1,1,1 };			// Light 0 Specular Values

	glLightfv(GL_LIGHT0, GL_POSITION, L0POS);

	glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard);

	// Run an Idle function repeatedly?
	glutIdleFunc(idle);


	// Orthographic 2D
	//gluOrtho2D(0,699,0,699); 
	
	// 3D Projection (Either Orthographic or Perspective.
	glMatrixMode(GL_PROJECTION);
	
	glLoadIdentity();
	//glOrtho(0, 1000, 0, 1000, 0, 1000);
	gluPerspective(45, 1, 1, 100);

	getNoOfSides(); // for regular polygon
	glutMainLoop(); // begin event-processing loop
	return 0;
}

void mouse(int button, int state, int x, int y)
{
	//myDisplay1();

	if (button == 0 && state == 00)
	{
		leftClick = !leftClick;
	}

	if (button == 2 && state == 00)
	{
		rightClick = !rightClick;
		clickedX = x;
		clickedY = y;
	}

	cout << "X:" << x << "   Y:" << y << endl;
	cout << leftClick << rightClick << endl;
	cout << button << endl;
	callbackDisplay();
}

void keyboard(unsigned char key, int x, int y)
{
	callbackDisplay();
}


void callbackDisplay()
{
	void example();
	void seriesOfLines();
	void polygon();
	void cube();
	void new3DCube();

	//	glClear(GL_COLOR_BUFFER_BIT); // Clear graphics window (to colour set by glClearColor)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear graphics window (to colour set by glClearColor)
	//example();

	if (leftClick)
		new3DCube();
	else if (rightClick)


	//seriesOfLines();	// Make a series of lines.
	//cube();
	glutSwapBuffers();
}

void idle() {

	moveit += increment;

	if (moveit > 15 || moveit < -20)
		increment *= -1;
	callbackDisplay();
}


void example()
{
	void new3DCube();

	rotation += 0.1;

	/*
	glColor3f(1,1,1); // set colour (white) for drawing commands (until next glColor3f call)
	glBegin(GL_POINTS); // draw a single point
		glVertex2i(300, 200);
	glEnd();
	glColor3f(0,0,1); // blue
	glBegin(GL_LINES); // draw a line
		glVertex2i(100,300);
		glVertex2i(150,100);
	glEnd();
	glLineWidth(15);
	glBegin(GL_LINES); // draw a line
	glVertex2i(600, 400);
	glVertex2i(50, 50);
	glEnd();
	glColor3f(1,1,1); // red
	glLineWidth(20);
	glBegin(GL_LINE_LOOP); // draw the edges of a polygon
		glVertex2i(500,300);

		glVertex2i(550,300);

		glVertex2i(520,250);
	glEnd();
	glColor3f(1,1,0); // yellow
	glBegin(GL_POLYGON); // draw a filled polygon
		glPointSize(10);
		glVertex2i(500,200);
		glVertex2i(550,200);
		glVertex2i(530,150);
	glEnd();
	*/
	// LAB 3.
	/*
	// YELLOW TRIANGLE.
	glPushMatrix();
	glColor3f(1, 1, 0);//yellow outline triangle
	//glTranslatef(-100, 0, 0);//translate 100 to the left

	glTranslatef(moveit, 0, 0);//translate 100 to the left
	glBegin(GL_LINE_LOOP); // draw the edges of a polygon
	glVertex2i(500, 300);
	glVertex2i(550, 300);
	glVertex2i(520, 250);
	glEnd();
	glPopMatrix();
	//glRotatef(20, 0, 0, 1);//rotate 20 degrees anticlockwise about origin



	// BLUE TRIANGLE
	glPushMatrix();
	glColor3f(0, 0, 1);//blue solid triangle
	// could push the matrix in future to isolate it.
	// MATRIX OPERATOINS (In Reverse Order Remember)
	glTranslatef(300, 200, 0);//translate back from origin
	glRotatef(45, 0, 0, 1);//rotate 45 degrees anticlockwise about origin
	glScaled(2, 2, 2);	// Scale it by 2 in X,Y and Z Axis
	glTranslatef(-300, -200, 0);//translate to origin
	// could pop the matrix, as if you don't pop it back, you're not maintaining the stack principle.
	glBegin(GL_POLYGON); // draw a filled polygon

	glVertex2i(300, 200);
	glVertex2i(350, 200);
	glVertex2i(330, 150);
	glEnd();
	glPopMatrix();
	*/
	// Lab 5.
	glPushMatrix();
	glTranslatef(0, 0, -10);				// Move the object back 10 in the Z axis.
	glRotatef(25.264 + rotation, 1, 0, 0);
	glRotatef(40 + rotation, 0, 1, 0);
	//glRotatef(0 + rotation, 0, 0, 1);

	new3DCube();
	glPopMatrix();
}



void getNoOfSides()
{
	cout << "How many sides? (Between 3 and 8, let's not get crazy...) ";
	cin >> noOfSides;
	while (noOfSides < 3 || noOfSides > 8)
	{
		cout << "Too few sides.\n";
		cout << "How many sides? ";
		cin >> noOfSides;
	}

	cout << endl << "Right click to set star position.  Left click to toggle Star or Polygon." << endl;
	cout << "Use Keys XXXXXX to rotate left and right." << endl;

	float internalAngle = 360 / noOfSides;

	for (int i(0); i < noOfSides; i++)
	{
		int xdistance = innerRadius * cos(internalAngle * (noOfSides+1));
		int ydistance = innerRadius * sin(internalAngle * (noOfSides + 1));
		
		PolygonInternalXPoints.push_back(xdistance);
		PolygonInternalYPoints.push_back(ydistance);
	}

	// Half way between the X axis and the first point. We make our first outer star point.
	float startingAngle = internalAngle * 0.5;

	for (int i(0); i < noOfSides; i++)
	{
		int xoutdistance = outerRadius * cos(startingAngle * (noOfSides + 1));
		int youtdistance = outerRadius * sin(startingAngle * (noOfSides + 1));

		PolygonExternalXPoints.push_back(xoutdistance);
		PolygonExternalYPoints.push_back(youtdistance);

		startingAngle += internalAngle;
	}

}

	bool written = false;
void polygon()
{

	if(!written)
	cout << "you're running the correct function at least";


	//glPushMatrix();
	//glTranslatef(clickedX, clickedY, 0);//translate somewhere
	//glBegin(GL_LINE_LOOP); // draw the edges of a polygon
						   //glPointSize(25);
						   //glLineWidth(25);



	float faceAmbient[] = { 0.2,0.2,0.2,1 };		// Light 0 Position
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, faceAmbient);
	float faceDiffuse[] = { 0.2,0.2,0.2,1 };		// Light 0 Position
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, faceDiffuse);
	float faceSpecular[] = { 0.2,0.2,0.2,1 };		// Light 0 Position
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, faceSpecular);

	float specularExponent(75);
	//glMaterialf(GL_FRONT_AND)


	//glBegin(GL_LINE_LOOP);
	glBegin(GL_TRIANGLES);
	glColor3f(0.7, 0.7, 0.7);

	for (int i(0); i < noOfSides; i++)
	{
		glVertex3i(PolygonInternalXPoints.at(i)+clickedX, PolygonInternalYPoints.at(i)+clickedY, 0);
		

		if (leftClick)
		{
			glVertex3i(PolygonExternalXPoints.at(i) +clickedX, PolygonExternalYPoints.at(i)+clickedY, 0);
		}
		if (!written)
		{
			cout << PolygonInternalXPoints.at(i)+clickedX << " " << PolygonInternalYPoints.at(i)+clickedY << endl;
			cout << PolygonExternalXPoints.at(i)+clickedX << " " << PolygonExternalYPoints.at(i)+clickedY << endl;
		}

	}
	written = true;
	glEnd();
	//glEnd();
	//glPopMatrix();

}

void seriesOfLines()
{
	//cout << "seriesOfLines called";
	const int noOfVertices(12);
	float vertices[][noOfVertices] =
	{
		{152, 75, 74,135,152,135,321,371,247, 75,152,371}, // x coords
		{256,324,149, 29,256, 29, 80,268,323,324,256,268}  // y coords
	};

	/*
	*/



	/*
	glPushMatrix();
	glTranslatef(300, 200 + moveit, 0);//translate somewhere
	glBegin(GL_LINE_LOOP); // draw the edges of a polygon
	//glPointSize(25);
	//glLineWidth(25);

	for (int i(0); i < noOfVertices; i++)
	{
		// first float (i)
		// second float (i+1)
		// draw a line between first and second float.
		glVertex2i(vertices[0][i], vertices[1][i]);
	}
	glEnd();
	glPopMatrix();
	*/

}

void cube()
{
	const int noOfVertices(8);
	const float perspVertices[4][noOfVertices] =  // homogeneous coordinates of vertices
	{
	  {-144.806, -54.133,-122.762, -32.089,  32.089, 122.762,  54.133, 144.806},
	  { -58.080,-142.871, 121.829,  37.038, -37.038,-121.829, 142.871,  58.080},
	  {   0    ,   0    ,   0    ,   0    ,   0    ,   0    ,   0    ,   0    },
	  {   1.150,   0.836,   0.981,   0.667,   1.332,   1.018,   1.163,   0.849}
	};
	const int verticesPerFace(4);
	const int faces[][verticesPerFace] = // vertex indices in visible faces
	{
	  {0, 1, 3, 2},
	  {7, 3, 1, 5},
	  {7, 6, 2, 3}
	};
	const int noOfFaces(sizeof(faces) / sizeof(faces[0])); // no. of rows in the matrix

	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////

	glPushMatrix();
	glTranslatef(300 + moveit, 400, 0);//translate somewhere

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < noOfFaces; facenumber++)
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = faces[facenumber][0];
		int vert2 = faces[facenumber][1];
		int vert3 = faces[facenumber][2];
		int vert4 = faces[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = perspVertices[0][vert1] / perspVertices[3][vert1];
		float point1y = perspVertices[1][vert1] / perspVertices[3][vert1];

		float point2x = perspVertices[0][vert2] / perspVertices[3][vert2];
		float point2y = perspVertices[1][vert2] / perspVertices[3][vert2];

		float point3x = perspVertices[0][vert3] / perspVertices[3][vert3];
		float point3y = perspVertices[1][vert3] / perspVertices[3][vert3];

		float point4x = perspVertices[0][vert4] / perspVertices[3][vert4];
		float point4y = perspVertices[1][vert4] / perspVertices[3][vert4];

		glColor3f(1, 1, 1);				// White
		glBegin(GL_LINE_LOOP);
		glVertex2i(point1x, point1y);
		glVertex2i(point2x, point2y);
		glVertex2i(point3x, point3y);
		glVertex2i(point4x, point4y);
		glEnd();
	}
	glPopMatrix();
}




void new3DCube()
{

	// 8 Verts in the Cube.
	const int howWide = 8;
	const int cubeVerts[][howWide] = // Because we don't say 3 in the first box, we can later use this for homogenous coords as well?
	{
	{-1, -1, 1, 1, -1, -1, 1, 1},
	{-1, 1, 1, -1, -1, 1, 1, -1},
	{1, 1, 1, 1, -1, -1, -1, -1}
	};

	// 6 Faces
	const int howWideNow = 6;
	const int facesOrder[][howWideNow] =
	{
	{0,3,2,1},	// Face 0
	{2,3,7,6},	// Face 1
	{3,0,4,7},	// Face 2
	{1,2,6,5},	// Face 3
	{4,5,6,7},	// Face 4
	{5,4,0,1}	// Face 5
	//...
	};

	const float colours[3][8] =
	{
		{1,0,0,0,1,1,1,1},
		{1,0,1,1,0,0,1,0},
		{1,1,0,1,0,1,0,0}
	};



	// Change the format over
	///////////////////////////////////////// MY ATTEMPT TO DRAW //////////////////////////////

	// For each face, get the numbers we need to draw.
	for (int facenumber(0); facenumber < howWideNow; facenumber++)	// Number of Faces.
	{
		// Which vertices are we dealing with for this face? Faces tells us.
		int vert1 = facesOrder[facenumber][0];
		int vert2 = facesOrder[facenumber][1];
		int vert3 = facesOrder[facenumber][2];
		int vert4 = facesOrder[facenumber][3];

		// For each vertices, we need an X and Y. And we need to extract that from homogenous coords.
		float point1x = cubeVerts[0][vert1];
		float point1y = cubeVerts[1][vert1];
		float point1z = cubeVerts[2][vert1];

		float point2x = cubeVerts[0][vert2];
		float point2y = cubeVerts[1][vert2];
		float point2z = cubeVerts[2][vert2];

		float point3x = cubeVerts[0][vert3];
		float point3y = cubeVerts[1][vert3];
		float point3z = cubeVerts[2][vert3];

		float point4x = cubeVerts[0][vert4];
		float point4y = cubeVerts[1][vert4];
		float point4z = cubeVerts[2][vert4];

		// This needs updating

		//glColor3f(1, 1, 1);				// White
		//glColor3f(colours[0][facenumber], colours[1][facenumber], colours[2][facenumber]);

		float faceAmbient[] = { 0.2,0.2,0.2,1 };		// Light 0 Position
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, faceAmbient);
		float faceDiffuse[] = { 0.2,0.2,0.2,1 };		// Light 0 Position
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, faceDiffuse);
		float faceSpecular[] = { 0.2,0.2,0.2,1 };		// Light 0 Position
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, faceSpecular);

		float specularExponent(75);
		//glMaterialf(GL_FRONT_AND)


		//glBegin(GL_LINE_LOOP);
		glBegin(GL_TRIANGLES);
		glColor3f(colours[0][vert1], colours[1][vert1], colours[2][vert1]);
		glVertex3f(point1x, point1y, point1z);
		glColor3f(colours[0][vert2], colours[1][vert2], colours[2][vert2]);
		glVertex3f(point2x, point2y, point2z);
		glColor3f(colours[0][vert3], colours[1][vert3], colours[2][vert3]);
		glVertex3f(point3x, point3y, point3z);
		glVertex3f(point3x, point3y, point3z);
		glColor3f(colours[0][vert4], colours[1][vert4], colours[2][vert4]);
		glVertex3f(point4x, point4y, point4z);
		glColor3f(colours[0][vert1], colours[1][vert1], colours[2][vert1]);
		glVertex3f(point1x, point1y, point1z);
		glEnd();

	}
	//		glPopMatrix();
}

